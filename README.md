# wp-bedrock-example

## Create bedrock project:
1) composer create-project roots/bedrock bedrock

## Installing plugins/themes
WordPress Packagist is already registered in the composer.json file so any plugins from the WordPress Plugin Directory can easily be required.

1) `cd bedrock`
2) `composer require wpackagist-plugin/akismet`

## Auto updates
This project has scheduled CI configured to check for updates. When updates are available, a merge request is made which is tested with CI.
The wordpress core won't be updated automatically, change composer.json as below to enable minor updates.

- "roots/wordpress": "5.7.2" > "roots/wordpress": "^5.7.2"
