FROM composer:2.0.13 as builder

COPY ./bedrock /bedrock
RUN cd /bedrock && composer install \
    --ignore-platform-reqs \
    --no-interaction

FROM bitnami/php-fpm:7.4-prod

# -- Wp cli 
RUN curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar \
    && chmod +x wp-cli.phar \
    && mv wp-cli.phar /usr/local/bin/wp

## -- Copy php.ini to config directory
COPY ./php/php.ini /opt/bitnami/php/etc/
COPY ./php/www.conf /opt/bitnami/php/etc/php-fpm.d
COPY ./php/zz-docker.conf /opt/bitnami/php/etc/php-fpm.d

## -- Fix log permissions
RUN chown -R www-data: /opt/bitnami/php/logs /opt/bitnami/php/tmp /opt/bitnami/php/var

# -- Change current user to www-data
USER www-data

COPY --chown=www-data:www-data --from=builder /bedrock /code
